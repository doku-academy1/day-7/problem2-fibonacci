public class Main {
    public static void main(String[] args) {
        System.out.println(fibonacci(5));
    }

    public static Integer fibonacci(Integer number) {
        if (number == 1) {
            return 0;
        } else if (number == 2) {
            return 1;
        }

        return fibonacci(number-1) + fibonacci(number-2);
    }
}